## gazebo_custom_sensor_preloader (noetic) - 1.1.0-1

The packages in the `gazebo_custom_sensor_preloader` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic --new-track gazebo_custom_sensor_preloader` on `Tue, 29 Aug 2023 15:43:32 -0000`

The `gazebo_custom_sensor_preloader` package was released.

Version of package(s) in repository `gazebo_custom_sensor_preloader`:

- upstream repository: https://github.com/ctu-vras/gazebo_custom_sensor_preloader
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `1.1.0-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


